'use strict';
const ChildProcess = require('child_process');
const mysql = require('mysql2/promise');

async function main() {
    const dockerID = await startDatabase();
    try {
        const connection = await setupDatabase();
        await test();
    } finally {
        stopDatabase(dockerID);
    }
}

async function test() {

}

/**
 * @return {string} Docker container ID
 */
async function startDatabase() {
    const command = 'docker run -e MYSQL_ALLOW_EMPTY_PASSWORD=yes -p 3306:3306 -d mysql:8.0';
    const output = ChildProcess.execSync(command);
    return output.slice(0, output.length - 2).toString();
}

/**
 * @param {string} id - Docker container ID
 */
function stopDatabase(id) {
    ChildProcess.execSync('docker stop ' + id);
}

/**
 * Connects to database and creates test tables.
 * @return {Promise<*>} MySQL connection
 */
async function setupDatabase() {
    const connection = await mysql.createConnection({
        host: 'localhost',
        user: 'root',
    }); // Error: connect ETIMEDOUT

    // TODO Create test tables

    return connection;
}

main();